## Модули

* BimboScanner - лексер
* TFAOParser - парсер для всех применений, использует BimboScanner
* TFModuleTrees - строит дерево разбора во время редактирования, парсит с помощью TFAOParser
* TFTypeSys - структуры данных. 

## Структуры данных

* Ident - идентификатор
* Scope - область видимости, например, модуль, процедура. Мы хотим добавить WITH, т.к. 
меняется тип.
* TFTypeSys.Designator - общий базовый класс для Ident, Index, Dereference, ActualParameters.
Процедура TFAOParser.Parser.Designator служит для того, чтобы его распарсить. Всегда начинается 
с имени, может быть `Имя[выражение]`, Имя.ИмяПоля.ИмяПоля2 , Имя->Имя2 и т.п.
* TFTypeSys.NamedObject - это TypeDecl, Const, Import, Var, ProcDecl, Class, Module
* TFModuleTrees.Reference - говорит, что в буфере по такому-то смещению находится такой-то NamedObject


## Переменные

* TFTypeSys.s - типа TFStringPool.StringPool
* TFAOParser.Parser.s - лексер

## Процедуры

### TFModuleTrees.ModuleTree.Refresh
TFModuleTrees.Mod, ф-я ModuleTree.Refresh - вызывает лексер, парсер, строит дерево разбора. Выжимки:
``` 
scanner := BimboScanner.InitWithText(editor.text, 0);
NEW(p); p.Parse(scanner); module := p.m;
(* TODO: check for parse errors *)
IF module # NIL THEN
	tree.SetNodeData(rootNode, GetTextInfo(module.name^, module.pos.a, module.pos.b, 0FFH, {WMGraphics.FontBold}, module));
	TraverseScope(rootNode, module.scope); (* строит таблицу узлов дерева модуля (которую будем показывать) для всех сущностей в тексте, например, классов и процедур *)
	SearchUses(); (* строит таблицу References, которая по смещению в буфере вернёт namedObject *)
```

### SearchUses - проходит по всему исходнику и каждому идентификатору назначает, где его определение.
Есть подпроцедура SearchStatements, которая проходит по телам кода. 


### TFAOParser.Parse
```
TFAOParser.Parser.Parse
   Module
     Body
       StatementBlock
         StatementSequence
           (* и там такой кусок: *)

	  |S.with : Next; designator := Designator(); Eat(S.colon); designator2 := Designator(); Eat(S.do);
	   sequence := StatementSequence(); Eat(S.end);
	   Add(TS.CreateWith(designator, designator2, sequence)) (* TS = TFTypeSys *)
  Или же
  Module
    DeclSeq(m.scope)
      S.procedure
        ProcDecl(scope)
          (DeclSeq - внутренние процедуры)
          Body - о как. 
      S.operator
```

### TFModuleTrees.FindIdentByPos - переходит к определению или часть перехода

На вход получает положение в буфере. Идёт по всем ModuleTree.references в модуле и находит тот Reference, к-рый расположен там, где курсор. Далее вызывает TFScopeTools.GetSourceReference и получает имя файла и scope stack. Далее вызывает TFPET.OnGoToDefinition -> TFPET.GotoDefinition -> TFPET.BrowseToDefinition

#### TFScopeTools.GetSourceReference - по NamedObject находит место его определения. 

#### TFModuleTrees.ModuleTree.BrowseToDefinition - переход к определению. 

На входе - путь к определению в виде "Модуль.Класс.Процедура" ... и т.п. Сначала отдельно находит модуль, потом в нём спускается по вложенным Scope, пока не доходит до NamedObject. Далее открывает с помощью
SelectNodeByNamedObject соответствующий узел дерева и переходит к нему. 

### См. также

[яп-активный-оберон/из-истории-fox-parser-и-fox-scanner.md](яп-активный-оберон/из-истории-fox-parser-и-fox-scanner.md)
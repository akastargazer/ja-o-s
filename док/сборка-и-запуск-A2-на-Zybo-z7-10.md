# Сборка и запуск A2 на Zybo-z7-10

## Подготовьте оборудование

Вам понадобится: 
- Zybo-z7-10
- Блок питания 5в 2.4а бочко-джек 21мм плюс внутри, например, GS15E-1P1J 
- Кабель USB - Micro USB
- HDMI-провод и монитор, который его понимает
- карта Micro SD, не все карты подходят, но карта с надписями
"Micro SD HC C 4 8 Gb C08G TAIWAN Kingston SDC4/8GB 14 F(C)" подошла (должна быть не ниже 4 класса скорости)
- устройство, с помощью которого вы запишете образ ОС на SD карту (в моём случае это SD ридер в компьютере под управлением Windows и переходник SD-MicroSD)

## Скачайте программы

- Зайдите в Windows (по Linux см. отдельный раздел в конце)
- Скачайте A2 с [SVN](цюрихский-репозиторий.md)
- Переключитесь на ревизию 9953
- Установите Win32 Disk Imager
- Установите Putty

## Запустите A2

- создайте директорию КореньРепозитория/Win64/Work
- выполните КореньРепозитория/Win64/a2.bat

## Соберите A2 

- Откройте лог ядра
- откройте файл с командами сборки PET.Open ARM/ZyboZ710.Tool ~
- найдите команду для порождения файла замен для режима 1024x768 и выполните её
- найдите команду System.DoCommands, после которой есть комментарий "Mount filesystems" и выполните её. 
- выполните следующую команду System.DoCommands, после неё идёт комментарий "Cleanup bootloader-specific files"
- через минуту или около того должно показаться сообщение "ZyboZ710 Zynq A2 image has been built!"
- должен возникнуть файл КореньРепозитория/Win64/Work/build/ZyboZ710.ZynqA2.img

## Запишите образ на SD карту

- с помощью программы Win32 Disk Imager запишите образ на SD карту (старые данные на карте пропадут)

## Подготовьте Zybo

В этом пункте, если в чём-то не уверены - читайте руководство по Zybo.

- Поставьте джампер JP6 в положение "WALL" (питание от адаптера)
- поставьте джампер JP5 в положение "SD"
- замкните контакты джампера JP2 (хотя это для данного опыта неважно)
- поставьте SD карту с записанным образом в разъём платы
- убедитесь, что выключатель питания находится в положении "Off"
- включите адаптер питания в сеть
- подключите адаптер питания
- подключите HDMI и монитор к левому разъёму (HDMI TX)
- включите монитор
- подключите шнур USB-MicroUSB к компьютеру и разъёму J12

## Запуск

- включите Zybo выключателем. Сразу должен загореться диод LD13 PGood, а через 5-10 секунд - 
диоды LD12 Done и LD6, и в это же время должна показаться картинка рабочего стола на экране.
- в "диспетчере устройств" найдите Порты(COM и LPT) и внутри должно быть что-то типа USB-Serial. Оттуда берёте номер COM-порта. Он появляется только при включённой Zybo, например, COM5
- запустите putty и задайте параметры:
    - тип соединения (connection type) - Serial
    - порт (serial line) - COM5
    - скорость (speed) -  115200
- далее в putty раскройте дерево свойств Connection/Serial и установите:
    - Data bits 8
    - Stop bits 1
    - Parity: none
    - Flow control: none (DTR/RTS enable)
- подключитесь (в дереве выберите session и нажмите кнопку Open)
- появится чёрный экран, нажмите Enter, должна появиться подсказка "Zynq A2 >"
- переключитесь в английскую раскладку и подайте команды, например, `PET.Open Configuration.XML` и нажмите Enter (не `~`). На экране должно появиться окно редактора. 
- мышь и клавиатура по идее должны подключаться через USB разветвитель, но они не работают
- сеть не работает. По идее, она должна бы заработать после команды XEmac.Install, которую нужно подавать при подключенном проводе Ethernet, но не заработала.
- всё (выключите питание Zybo и отключите все проводочки)

## Linux

Могут помочь фрагменты нижеследующего потока сознания:
```
Здесь речь идёт не о запуске A2, а просто о том, чтобы хоть какого-то толка добиться от неё. Вот примерный журнал действий (до журнала очень далеко, но хотя бы указаны ссылки, которые использовались и область поиска). 

1. Заводим учётную запись xilinx
2. Загружаем petalinux. Устанавливаем. Говорит, что нет ncurses. Гуглим petalinux ubuntu ncurses и находим документ, в котором есть табличка, какие пакеты нужно поставить для какой компоненты. 
Заходим в директорию, в к-рой будем ставить, и указываем путь к .run файлу. Он пугает нас, что директория непуста, хотя она пуста. Пропускаем. После этого нет файлов petalinux-*, хз, где они.

Потом находим их:

куда-устанавливали/tools/common/petalinux/bin - видимо, нужно вручную добавить к PATH, но их там
немало. РТФМ:

https://www.xilinx.com/support/documentation/sw_manuals/xilinx2020_2/ug1144-petalinux-tools-reference-guide.pdf

Оказывается, нужно запустить
source куда-устанавливали/settings.sh


2. Загружаем vitis (той же версии)
После загрузки ему надо сделать chmod u+x Xilinx_Unified_2020.2_1118_1232_Lin64.bin
Он потребует учётку. Неясно, что устанавливать - попробуем установить Vivaldo. Там нужно выбрать WebPack (он будет сливать данные с вашего компьютера). 

Убираем все устройства, кроме Production Devices/SoCs/Zynq-7000

Пока Vivaldo+Vitis грузятся (они огромные), находим, что нам нужен bsp. Гуглим его и находим тут:

https://github.com/Digilent/Petalinux-Zybo-Z7-10

Клонируем его, переключаемся на релизную ветку. 

cd /y/pl1
git clone --recursive https://github.com/Digilent/Petalinux-Zybo-Z7-10.git - оно тоже затпуило... 

===================================

Тем временем пытаемся решить вопрос с доступностью (недоступностью)  сайта digilent (видимо, Россия занесена в чёрный список). Для начала пытаемся скачать Оперу... Она тоже качается... Больше уж нечего делать, помыть посуду, что ли? Нет... Опера скачалась, поставилась, настройки/дополнительно/конфиденциальность/vpn разрешить в настройках/жмём, открывается другая страница, там разрешаем, и вроде что-то работает. 

Теперь разбираемся с джампером питания (руководство по zybo,  кстати, есть и на сайте xilinx) - он стоит в положении "usb" с завода, а у нас и есть usb. С этим ок. 

https://www.youtube.com/watch?v=QWwOsh0L7BQ - тут показывают физические подключения, похоже, что на турецком. Xillinux by XILLYBUS - о! И нужен USB HUB. Но на страничке xillybus написано, что zybo не поддерживается. Идём дальше... 


Наконец-то скачался бинарный bsp файл (хаха, для версии 2017 - хорошего не ждём!), указанный на гитхабе. Притом, его нет в репозитории (зря мы его клонировали), в репозитории есть лишь ссылка на него. 

Монтируем SD-карту:
https://ragnyll.gitlab.io/2018/05/22/format-a-sd-card-to-fat-32linux.html

копируем то, что сказано в Petalinuz-Zybo-Z7-10.git (скачав bsp файл, а сборка при этом не сработала, т.е. только бинарный файл можно взять). 

дальше подключаем, как там сказано (и включаем плату - об этом не сказано!)

subo minicom -s /dev/ttyUSB1

Порт может быть и ttyUSB0 - он появляется только после включения платы.

- отключаем аппаратное управление, остальное там норм. Выходим. Далее

sudo minicom /dev/ttyUSB1

wirenboard.com/wiki/Работа_с_последовательным_портом_из_Linux


никак. Теперь устанавливаем драйвера для кабеля:

https://www.xilinx.com/support/answers/59128.html?_ga=2.268427657.600683315.1614370176-2030655243.1614370176 


Также смотрим это:

https://forum.digilentinc.com/topic/17321-zybo-z7-serial-port-does-not-output-hello-world-message-when-it-does-output-zybo-z7-20-rev-b-demo-image/

и там ещё говорят установить некий adept2 - но там нужно установить ДВА пакета и это не совсем очевидно (runtime и utils)

после включения sudo djtgcfg enum показал наличие устройства :) 

в руководстве написано, что джампер управления загрузкой должен находиться в положении sd для загрузки с sd - переставляем (по умолчанию в другом положении).

При этом, на подключённом к левому HDMI экране через некоторое время появилась подсказка линукса, а сначала был чёрный экран. Если бы в USB была воткнута клавиатура (при положении JP2 = host), то она бы заработала и можно было бы ввести логин/пароль.

Но minicom ничего не показывает. Для того, чтобы заработал minicom, внезапно помогло переткнуть провод в другое гнездо в компьютере - как и было обещано, появилась консоль линукса в миникоме и даже удалось выполнить команду shutdown -h now.

Также, возможнО, что команда на самом деле должна быть minicom -b 115200 -D /dev/ttyUSB1
```

                                                                      
    


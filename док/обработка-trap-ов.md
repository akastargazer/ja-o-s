# Обработка TRAP-ов

Цель статьи - изучить TRAPы, чтобы уметь использовать их в программировании. Например, в среде разработки, если текст некорректен, использовать TRAP для сообщения
об этом.

## Элементы кода

Платформа.Traps.Mod - модуль Traps, платформо-зависимый.

* Traps.Show - показать исключение
* Traps.GetLastExceptionState - ?
* Traps..Exception - обработчик исключения (не экспортирован)
* Machine.InstallHandler - установить обработчик исключения (например, Traps..Exception)
* TrapWriters.TrapWriter - видимо, это объект, который печатает трапы в разные места, там можно и отключить вывод трапов.
* TrapWriters.WriterFactory = PROCEDURE (): Streams.Writer;
* TrapWriters.InstallTrapWriterFactory - печатать трапы ещё и в указанное место. Но есть и
* TrapWriters.UninstallTrapWriterFactory

## Objects.halt и Objects.haltUnbreakable

Я так примерно понял, что haltUnbreakable более жёстко убивает процесс. Его можно увидеть в ГПИ в инструменте Inspect/Objects.
В обоих случаях пытаемся выполнить FINALLY, но в haltUnbreakable их тоже выполняем как-то более агрессивно.

## Откуда берётся красный экран?

Он настроен в Configuration.XML

```
<Section name = "Autostart">
<Setting name = "FileTrapWriter" value = "FileTrapWriter .Install "/>
<Setting name = "TrapWriter" value = "WMTrapWriter .Install "/>
```

FileTrapWriter.Install делает так, что для каждого трапа создаётся файл Trap_....txt в рабочей директори (напр., WinAOS)
WMTrapWriter показывает красный экран.

Соответственно, выполнение

```
WMTrapWriter .Uninstall
```

полностью отключает красный экран.

## Идеи

Иногда может захотеться <<молчаливое исключение>>, которое обрабатывается, но не показывается.
Решение - в ветке silent-trap-7777, общая идея - в том, что для выбранного кода исключения 7777 никакое отображение не происходит.

```
diff --git "a /C :\\Users\\ASUS\\AppData\\Local\\Temp\\TortoiseGit\\Win32 .Traps -bc673e6 . 000 .Mod " "b /C :\\Users\\ASUS\\AppData\\Local\\Temp\\TortoiseGit\\Win32 .Traps -b6e7f36 . 000 .Mod "  
index f1dd4ceb8f ..e0db336ed7 100644  
---"a /C :\\Users\\ASUS\\AppData\\Local\\Temp\\TortoiseGit\\Win32 .Traps -bc673e6 . 000 .Mod "  
+++ "b /C :\\Users\\ASUS\\AppData\\Local\\Temp\\TortoiseGit\\Win32 .Traps -b6e7f36 . 000 .Mod "  
@@-339 ,7 +339 ,9 @@VAR  
*)  
traceTrap :=FALSE ;  
  
-Show (t , int , exc ,(* exc.halt # MAX(INTEGER)+1*)TRUE );(* Always show the trap info!*)  
+IF exchalt # 7777 (*Invisible trap*)THEN  
+Show (t , int , exc ,(* exc.halt # MAX(INTEGER)+1*)TRUE );(* Always show the trap info!*)  
+END ;  
  
IF exchalt =haltUnbreakable THEN Unbreakable (t , int , exc , handled )  
ELSIF ~traceTrap THEN HandleException (int , exc , handled )  
```

См. также [https://forum.oberoncore.ru/viewtopic.php?f=22&t=6468](https://forum.oberoncore.ru/viewtopic.php?f=22&t=6468)

MODULE BtDecoder;
	IMPORT SYSTEM, Modules, Streams, MemoryReader, Strings, Files, KernelLog, TextUtilities, Texts;
CONST 
	maxDecoders = 5; 
	MaxOpcodeSize = 20; 
	OFFHdrRef = 8CX; 
TYPE 
	Opcode* = OBJECT 
	VAR 
		instr*: SIGNED32; 
		offset*: SIGNED32; 
		code*: Modules.Bytes; 
		length-: SIGNED32; 
		decoder*: Decoder; 
		next*: Opcode; 
		stream: Streams.Writer; 
		proc-: ProcedureInfo; 

		PROCEDURE ^  & New*(proc: ProcedureInfo; stream: Streams.Writer); 
		PROCEDURE ^ PrintOpcodeBytes*(w: Streams.Writer); 
		PROCEDURE ^ PrintInstruction*(w: Streams.Writer); 
		PROCEDURE ^ PrintArguments*(w: Streams.Writer); 
		PROCEDURE ^ PrintVariables*(w: Streams.Writer); 
		PROCEDURE ^ ToString*(): Strings.String; 
		PROCEDURE ^ WriteHex8*(x: SIGNED32; w: Streams.Writer); 
		PROCEDURE ^ WriteHex16*(x: SIGNED32; w: Streams.Writer); 
		PROCEDURE ^ WriteHex32*(x: SIGNED32; w: Streams.Writer); 
	END Opcode; 

	Decoder* = OBJECT {EXCLUSIVE} 
	VAR 
		codeBuffer: Modules.Bytes; 
		reader: Streams.Reader; 
		outputStreamWriter*: Streams.Writer; 
		firstOpcode, lastOpcode, currentOpcode: Opcode; 
		completed: BOOLEAN; 
		currentBufferPos, currentCodePos, opcodes, mode: SIGNED32; 
		currentProc*: ProcedureInfo; 

		PROCEDURE ^  & New*(reader: Streams.Reader); 
		PROCEDURE ^ Bug*(op, no: SIGNED32); 
		PROCEDURE ^ NewOpcode*(): Opcode; 
		PROCEDURE ^ DecodeThis*(opcode: Opcode); 
		PROCEDURE ^ Decode*(proc: ProcedureInfo): Opcode; 
		PROCEDURE ^ CopyBufferToOpcode(opcode: Opcode); 
		PROCEDURE ^ InsertBytesAtBufferHead*(bytes: Modules.Bytes); 
		PROCEDURE ^ ReadChar*(): CHAR; 
		PROCEDURE ^ ReadInt*(): SIGNED16; 
		PROCEDURE ^ ReadLInt*(): SIGNED32; 
	END Decoder; 

	DecoderFactory = PROCEDURE {DELEGATE}(reader: Streams.Reader):Decoder; 

	Info = OBJECT 
	VAR 
		name-: ARRAY 256 OF CHAR; 
	END Info; 

	FieldInfo* = OBJECT (Info)
	END FieldInfo; 

	FieldArray = POINTER TO ARRAY OF FieldInfo; 

	ProcedureInfo* = OBJECT (Info)
	VAR 
		codeOffset, codeSize, retType-, index: SIGNED32; 
		fields: FieldArray; 
		fieldCount-: SIGNED32; 
		method: BOOLEAN; 
		gcInfo: GCInfo; 

		PROCEDURE ^  & New(CONST n: ARRAY OF CHAR; ofs, idx: SIGNED32); 
		PROCEDURE ^ GetFieldAtOffset*(offset: SIGNED32): FieldInfo; 
	END ProcedureInfo; 

	ProcedureArray = POINTER TO ARRAY OF ProcedureInfo; 

	TypeInfo* = OBJECT (Info)
	VAR 
		procedures: ProcedureArray; 
		fields: FieldArray; 
		procedureCount, fieldCount: SIGNED32; 

		PROCEDURE ^  & New(CONST n: ARRAY OF CHAR); 
	END TypeInfo; 

	TypeArray = POINTER TO ARRAY OF TypeInfo; 

	Export* = POINTER TO RECORD 
		next: Export; 
		fp: SIGNED32; 
		val: SIGNED32; 
	END; 

	Use = POINTER TO RECORD 
		next: Use; 
		fp: SIGNED32; 
		val: SIGNED32; 
		name: ARRAY 256 OF CHAR; 
	END; 

	Import = OBJECT 
	VAR 
		next: Import; 
		name: ARRAY 256 OF CHAR; 
		uses: Use; 

		PROCEDURE ^ AddUse(u: Use); 
	END Import; 

	VarConstLink = RECORD 
		num: SIGNED32; 
		ch: CHAR; 
		links: POINTER TO ARRAY OF SIGNED32; 
	END; 

	Link = RECORD 
		num: SIGNED32; 
	END; 

	Entry = RECORD 
		num: SIGNED32; 
	END; 

	GCInfo = POINTER TO RECORD 
		codeOffset, beginOffset, endOffset: SIGNED32; 
		pointers: POINTER TO ARRAY OF SIGNED32; 
	END; 

	ObjHeader = RECORD 
		entries, commands, pointers, types, modules: SIGNED32; 
		codeSize, dataSize, refSize, constSize, exTableLen, crc: SIGNED32; 
	END; 

	ModuleInfo* = OBJECT (Info)
	VAR 
		module: Modules.Module; 
		header: ObjHeader; 
		procedures: ProcedureArray; 
		procedureCount: SIGNED32; 
		types: TypeArray; 
		typeCount: SIGNED32; 
		currentProcInfo: ProcedureInfo; 
		ext: Extension; 
		codeScaleCallback: CodeScaleCallback; 
		exports: Export; 
		imports: Import; 
		varConstLinks: POINTER TO ARRAY OF VarConstLink; 
		links: POINTER TO ARRAY OF Link; 
		entries: POINTER TO ARRAY OF Entry; 
		gcInfo: POINTER TO ARRAY OF GCInfo; 

		PROCEDURE ^ AddExport(e: Export); 
		PROCEDURE ^ AddImport(i: Import); 
		PROCEDURE ^ GetOpcodes*(proc: ProcedureInfo): Opcode; 
		PROCEDURE ^ AddProcedure(procInfo: ProcedureInfo); 
		PROCEDURE ^ GetProcedureByIndex*(idx: SIGNED32): ProcedureInfo; 
		PROCEDURE ^ DecodeRefs(reader: Streams.Reader); 
		PROCEDURE ^ FindProcedureFromPC*(pc: UNSIGNED32): ProcedureInfo; 
		PROCEDURE ^ Init; 
		PROCEDURE ^ OutlineNamedProcedure*(CONST name: ARRAY OF CHAR; w: Streams.Writer); 
		PROCEDURE ^ OutlineProcedure*(proc: ProcedureInfo; w: Streams.Writer); 
	END ModuleInfo; 

	CodeScaleCallback* = PROCEDURE (VAR size: SIGNED32); 

	ModuleInfoObjectFile* = OBJECT (ModuleInfo)
	VAR 
		f: Files.File; 
		r: Files.Reader; 
		version: SIGNED32; 
		nofLinks, nofVarConstLinks: SIGNED32; 
		symSize: SIGNED32; 
		noProcs: SIGNED32; 

		PROCEDURE ^ DecodeEntries; 
		PROCEDURE ^ SkipCommands; 
		PROCEDURE ^ SkipPointers; 
		PROCEDURE ^ SkipImports; 
		PROCEDURE ^ SkipVarConstLinks; 
		PROCEDURE ^ SkipLinks; 
		PROCEDURE ^ SkipConsts; 
		PROCEDURE ^ SkipExports; 
		PROCEDURE ^ SkipUse; 
		PROCEDURE ^ DecodeTypes; 
		PROCEDURE ^ DecodeExTable(r: Streams.Reader); 
		PROCEDURE ^ SkipPointerInProc; 
		PROCEDURE ^  & New*(CONST fileName: ARRAY OF CHAR); 
	END ModuleInfoObjectFile; 

	Extension = ARRAY 4 OF CHAR; 

	DecoderType = OBJECT 
	VAR 
		ext: Extension; 
		decoderFactory: DecoderFactory; 
		codeScaleCallback: CodeScaleCallback; 

		PROCEDURE ^  & New(CONST ext: Extension; decoderFactory: DecoderFactory; codeScaleCallback: CodeScaleCallback); 
	END DecoderType; 
VAR 
	decoderTypes: ARRAY maxDecoders OF DecoderType; 
	nofDecoders: SIGNED32; 
	lastExt: Extension; 

	PROCEDURE ^ IntToHex*(h, width: SIGNED32; VAR s: ARRAY OF CHAR); 
	PROCEDURE ^ GetDecoderType*(CONST ext: Extension): DecoderType; 
	PROCEDURE ^ GetDecoder(CONST ext: Extension; reader: Streams.Reader): Decoder; 
	PROCEDURE ^ RegisterDecoder*(CONST ext: Extension; decFactory: DecoderFactory; csclCallback: CodeScaleCallback); 
	PROCEDURE ^ GetCodeScaleCallback(CONST ext: Extension): CodeScaleCallback; 
	PROCEDURE ^ Initialize(CONST decoder: ARRAY OF CHAR); 
	PROCEDURE ^ Test*; 
BEGIN
END BtDecoder.

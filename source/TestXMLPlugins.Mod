модуль TestXMLPlugins;	(** AUTHOR "Simon L. Keel"; PURPOSE "example-plugins for the XMLTransformer"; *)

использует
	XMLTransformer, Strings, XML, WebBrowserComponents;

тип
	String = Strings.String;

перем
	newLine : ряд 3 из литера8;

проц A*(elem : XML.Element) : XML.Container;
перем
	container : XML.Container;
	p, sp : XML.Element;
	cdata : XML.CDataSect;
	s : String;
тело
	нов(container);

	p := XMLTransformer.GetNewParagraph("Center");
	sp := XMLTransformer.GetNewSpan("Assertion");
	нов(cdata);
	s := Strings.ConcatToNew("This is example-output of BB:A tag", newLine);
	cdata.SetStr(s^);
	sp.AddContent(cdata);
	p.AddContent(sp);
	container.AddContent(p);

	возврат container;
кн A;

проц AB*(elem : XML.Element) : XML.Container;
перем
	container : XML.Container;
	e : XML.Element;
тело
	нов(container);

	нов(e); e.SetName("BB:A");
	XMLTransformer.AddContentsOf(XMLTransformer.Transform(e), container);

	XMLTransformer.AddContentsOf(XMLTransformer.TransformElemsIn(elem), container);

	нов(e); e.SetName("BB:B");
	XMLTransformer.AddContentsOf(XMLTransformer.Transform(e), container);

	возврат container;
кн AB;

проц B*(elem : XML.Element) : XML.Container;
перем
	container : XML.Container;
	p, sp : XML.Element;
	cdata : XML.CDataSect;
	s : String;
тело
	нов(container);

	p := XMLTransformer.GetNewParagraph("Left");
	sp := XMLTransformer.GetNewSpan("Preferred");
	нов(cdata);
	s := Strings.ConcatToNew("This is example-output of the BB:B tag", newLine);
	cdata.SetStr(s^);
	sp.AddContent(cdata);
	p.AddContent(sp);
	container.AddContent(p);

	возврат container;
кн B;

проц PICT*(elem : XML.Element) : XML.Container;
перем
	container : XML.Container;
	p, o : XML.Element;
	img : WebBrowserComponents.StretchImagePanel;
тело
	нов(container);

	p := XMLTransformer.GetNewParagraph("Left");
	нов(o);
	o.SetName("Object");
	нов(img, НУЛЬ, Strings.NewString("file://BluebottlePic0.png"), -1, -1);
	o.AddContent(img);
	p.AddContent(o);
	container.AddContent(p);

	возврат container;
кн PICT;

тело
	newLine[0] := 0DX; newLine[1] := 0AX; newLine[2] := 0X;
кн TestXMLPlugins.

XMLTransformer.Register A TestXMLPlugins.A ~
XMLTransformer.Register AB TestXMLPlugins.AB ~
XMLTransformer.Register B TestXMLPlugins.B ~
XMLTransformer.Register PICT TestXMLPlugins.PICT ~

Example-HTML-File:

<html>
<body background="paper.gif">

<h1>XML-Transfomer-Demo</h1>

<h2>The BB:A tag</h2>
<BB:A />
<hr>

<h2>The BB:B tag</h2>
<BB:B />
<hr>

<h2>The BB:PICT tag</h2>
<BB:PICT />
<hr>

<h2>The BB:AB tag when empty</h2>
<BB:AB />
<hr>

<h2>The BB:AB tag when containing a BB:PICT tag</h2>
<BB:AB>
   <BB:PICT />
</BB:AB>
<hr>

</body>
</html>

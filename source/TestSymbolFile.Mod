MODULE TestSymbolFile; (** AUTHOR ""; PURPOSE ""; *)
IMPORT Strings,hny;

TYPE
	String= Strings.String;
	TestRec = RECORD
		tint:SIGNED32;
		treal:FLOAT32;
		tarr1:ARRAY 10 OF SIGNED32;
		tarr2:ARRAY 10,10 OF FLOAT32;
		tarr3:ARRAY 80 OF CHAR;
	END;
	
	TestRecEx* = RECORD(TestRec)
		adds1*:Strings.String;
		adds2*:String;
	END;
	
	TestObj* = OBJECT
		VAR
			oint*:SIGNED32;
			oreal*:FLOAT32;
			orec:TestRec;
			orecex:TestRecEx;
			oarr1*:ARRAY 10 OF SIGNED32;
			oarr2:ARRAY 10,10 OF FLOAT32;
			oarr3*:ARRAY 80 OF CHAR;
		(** *)
		PROCEDURE &Init;
		VAR
		BEGIN
			
		END Init;
		
	END TestObj;
VAR
 t1:TestRec;
 t2:TestRecEx;
 t3:TestObj;
BEGIN
 t2.tint:=5;
END TestSymbolFile.

FoxBinarySymbolFile.Test   --symbolFileExtension=".Obw" TestSymbolFile    ~
FoxBinarySymbolFile.Test   --symbolFileExtension=".Obw" WMTextView    ~

Compiler.Compile --objectFile=Generic --newObjectFile -b=AMD --objectFileExtension=.GofW --symbolFileExtension=.SymW  WMDebugger/TestSymbolFile.Mod~